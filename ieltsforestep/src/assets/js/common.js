// JavaScript Document
jQuery(document).ready(function() {



jQuery('#horizontalTab').easyResponsiveTabs({
		type: 'default', //Types: default, vertical, accordion           
		width: 'auto', //auto or any width like 600px

	});
	
	jQuery('#verticalTab').easyResponsiveTabs({
		type: 'default', //Types: default, vertical, accordion           
		width: 'auto', //auto or any width like 600px

	});
	jQuery("#horizontalTab .tab-content").mCustomScrollbar();
/*Header fixed*/
 if(jQuery(window).width() > 1024)
 {
	 jQuery(window).bind("scroll", function() {
             if (jQuery(window).scrollTop() >= 48) {
                 jQuery(".header-wrapper").addClass("fixed");
             } else {
                jQuery(".header-wrapper").removeClass("fixed");

             }
         });
}


/*Navigation*/

jQuery(".review-slider").slick({
        			autoplay: false,
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 1,
  arrows: true,
  dots: false,
responsive: [
{
breakpoint: 1280,
settings: {
slidesToShow: 4,
slidesToScroll: 1
}
},

{
breakpoint: 760,
settings: {
slidesToShow: 3,
slidesToScroll: 1
}
},
{
breakpoint: 480,
settings: {
slidesToShow: 1,
slidesToScroll: 1
}
}
]
    });

	
	jQuery(".process-slider").slick({
        			autoplay: true,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: true,
  dots: false,
responsive: [
{
breakpoint: 1280,
settings: {
slidesToShow: 4,
slidesToScroll: 1
}
},

{
breakpoint: 760,
settings: {
slidesToShow: 3,
slidesToScroll: 1
}
},
{
breakpoint: 480,
settings: {
slidesToShow: 1,
slidesToScroll: 1
}
}
]
    });


jQuery(".work-slider").slick({
        			autoplay: true,
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows: true,
  dots: false,
responsive: [
{
breakpoint: 1280,
settings: {
slidesToShow: 4,
slidesToScroll: 1
}
},

{
breakpoint: 760,
settings: {
slidesToShow: 3,
slidesToScroll: 1
}
},
{
breakpoint: 480,
settings: {
slidesToShow: 1,
slidesToScroll: 1
}
}
]
    });
	
	/*banner-slider*/

		
		
	/*scrollingArrow*/	
		jQuery('.scrollingArrow-anchor').on( 'click', function(){
				jQuery('html, body').animate({
					scrollTop: jQuery(".land").offset().top-88
				}, 2000);
			});
		
	
			
	
jQuery('.logo-inner').slick({
			autoplay: false,
  infinite: true,
  slidesToShow: 6,
  slidesToScroll: 1,
  arrows: true,
  dots: false,
responsive: [
{
breakpoint: 1280,
settings: {
slidesToShow: 4,
slidesToScroll: 1
}
},

{
breakpoint: 760,
settings: {
slidesToShow: 3,
slidesToScroll: 1
}
},
{
breakpoint: 480,
settings: {
slidesToShow: 1,
slidesToScroll: 1
}
}
]
        });

	
   
});