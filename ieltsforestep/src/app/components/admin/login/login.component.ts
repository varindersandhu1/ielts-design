import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AccountService } from 'src/app/services/account-service';
import { NotificationService } from 'src/app/services/notification-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  constructor( private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private accountService:AccountService,
    private  notificationService :  NotificationService 
    ) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      username: new FormControl(''),
      password: new FormControl(''),
    });
  }

  get f() { return this.loginForm.controls; }

    onSubmit() {
      this.submitted = true;
      console.log(this.loginForm);
      localStorage.setItem('login_admin', this.loginForm.value.username);
      // stop here if form is invalid

       console.log(this.loginForm.value);
     //  this._loader = true;
           this.accountService.AdminLogin(this.loginForm.value).subscribe(response=>{
          //  this._loader = false;
            if(response.Status){
              localStorage.setItem('AuthToken', response.Result[0].TokenKey);
              localStorage.setItem('TypeName', response.Result[0].TypeName);
              localStorage.setItem('UserID', response.Result[0].UserID);
              this.router.navigate(["/admin/profile"]);
              //this.router.navigate(["/admin/catelogs"]);
            }else{
              this.notificationService.showError("Employee Code or Pass Key Invalid")
          }}
           )
      this.router.navigate(["/admin/profile"]);
  }
}
