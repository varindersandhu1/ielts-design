import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators ,ReactiveFormsModule } from '@angular/forms';
import { AccountService } from 'src/app/services/account-service';
import { NotificationService } from 'src/app/services/notification-service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss']
})
export class UserLoginComponent implements OnInit {

  _fireloginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  IsCheckLogin : string;
_loader=false;
  constructor( private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private accountService:AccountService,
    private router: Router,
    private  notificationService :  NotificationService 
    ) { }

  ngOnInit() {
    this._fireloginForm = new FormGroup({
      EmployeeCode: new FormControl(''),
      PassKey: new FormControl(''),
    });


            if (localStorage.getItem('AuthToken')) {
                this.router.navigate(["/admin/catelogs"]);
            }

  }

  get f() { return this._fireloginForm.controls; }
    onSubmit() {
      this.router.navigate(["/user/home"]);
      // console.log(this._fireloginForm.value);
      //   this._loader = true;
      //     this.accountService.AdminLogin(this._fireloginForm.value).subscribe(response=>{
      //       this._loader = false;
      //       if(response.Status){
      //         localStorage.setItem('AuthToken', response.Result[0].TokenKey);
      //         localStorage.setItem('TypeName', response.Result[0].TypeName);
      //         localStorage.setItem('UserID', response.Result[0].UserID);
      //         this.router.navigate(["/admin/catelogs"]);
      //       }else{
      //         this.notificationService.showError("Employee Code or Pass Key Invalid")
      //       }}
      //       )
            }
}
