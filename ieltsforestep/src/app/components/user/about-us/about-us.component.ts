import { Component, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Subscription } from 'rxjs'
@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  
  constructor(private router: Router,private AuthenticationService : AuthenticationService) { 
  }

  ngOnInit() {
    this.AuthenticationService.getValue("about-us"); 
  }

  // getInfo(){
  //   debugger;
  //     this.ShowHomePageBanner =  this.router.url.split("/")[2];
  //     console.log(this.ShowHomePageBanner);
  //     this.AuthenticationService.getValue("about-us"); 
  // }

}
