import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserAppComponent } from './layout/user/app/app.component';
import { AdminAppComponent } from './layout/admin/app/app.component';
import { LoginComponent } from './components/admin/login/login.component';
import { UserLoginComponent } from './components/user/user-login/user-login.component';





const routes: Routes = [
	{
		path: 'auth',
	//	component: AuthAppComponent,

		children: [
			{ path: 'admin-login', component: LoginComponent },
			{ path: 'login', component: UserLoginComponent },
		]
	},
	{
		path: 'user',
		component: UserAppComponent,
    loadChildren: () => import('./layout/user/app/user.module')
    .then(m => m.UserModule),
  },
  
  {
		path: 'admin',
		component: AdminAppComponent,
		loadChildren: () => import('./layout/admin/app/admin.module')
    .then(m => m.AdminModule),
	},


 	
	{ path: '**', redirectTo: 'user' }
];

@NgModule({

  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
