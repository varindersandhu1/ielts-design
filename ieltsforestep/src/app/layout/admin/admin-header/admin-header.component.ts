import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss']
})
export class AdminHeaderComponent implements OnInit {

  IsAdminlogin = false;


  constructor(private router:Router) { }

  ngOnInit() {

    if (localStorage.getItem('login_admin')) {
      this.IsAdminlogin = true;
    }else{
      this.IsAdminlogin = false;
    }

  }



  LogoutAdmin(){

    localStorage.removeItem('login_admin');
    this.router.navigate(["auth/login"]);

  }

}
