import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

  

const adminRoutes: Routes = [
			{ path: '**', redirectTo: 'dashboard' }
];

  


@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
