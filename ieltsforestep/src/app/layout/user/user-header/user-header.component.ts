import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user-header',
  templateUrl: './user-header.component.html',
  styleUrls: ['./user-header.component.scss']
})
export class UserHeaderComponent implements OnInit {
  ShowHomePageBanner: any;
  constructor(private router: Router,private AuthenticationService : AuthenticationService) {
    this.AuthenticationService.testcall.subscribe(()=>{
      this.ShowHomePageBanner =  this.router.url.split("/")[2];
    })

  }
  ngOnInit(){
  }
}