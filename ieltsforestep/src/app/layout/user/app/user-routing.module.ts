import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from 'src/app/components/user/home/home.component';
import { AboutUsComponent } from 'src/app/components/user/about-us/about-us.component';
import { UserSignUpComponent } from 'src/app/components/user/user-sign-up/user-sign-up.component';
import { CategoriesComponent } from 'src/app/components/user/categories/categories.component';
import { ReadingComponent } from 'src/app/components/user/reading/reading.component';
import { WritingComponent } from 'src/app/components/user/writing/writing.component';
import { ListeningComponent } from 'src/app/components/user/listening/listening.component';
import { SpeakingComponent } from 'src/app/components/user/speaking/speaking.component';
 


const userRoutes: Routes = [
      { path: 'home', component: HomeComponent },
      { path: 'categories', component: CategoriesComponent },
      { path: 'reading', component: ReadingComponent },
      { path: 'writing', component: WritingComponent },
      { path: 'listening', component: ListeningComponent },
      { path: 'speaking', component: SpeakingComponent },
      { path: 'about-us', component: AboutUsComponent },
      { path: 'sign-up', component: UserSignUpComponent },
			{ path: '**', redirectTo: 'home' }
];

@NgModule({
  declarations: [
    HomeComponent,
    AboutUsComponent,
    UserSignUpComponent,
    CategoriesComponent,
    ReadingComponent,
    WritingComponent,
    ListeningComponent,
    SpeakingComponent,
  ],


  imports: [RouterModule.forChild(userRoutes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
