import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserAppComponent } from './layout/user/app/app.component';
import { AdminAppComponent } from './layout/admin/app/app.component';
import { AdminDashboardComponent } from './components/admin/admin-dashboard/admin-dashboard.component';
import { AdminHeaderComponent } from './layout/admin/admin-header/admin-header.component';
import { AdminFooterComponent } from './layout/admin/admin-footer/admin-footer.component';
import { UserHeaderComponent } from './layout/user/user-header/user-header.component';
import { UserFooterComponent } from './layout/user/user-footer/user-footer.component';
import { AdminSideMenuComponent } from './layout/admin/admin-side-menu/admin-side-menu.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { LoginComponent } from './components/admin/login/login.component';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { UserLoginComponent } from './components/user/user-login/user-login.component';
import { HttpClientModule, /* other http imports */ } from "@angular/common/http";



           

@NgModule({
  declarations: [
    AppComponent,
    AdminAppComponent,
    UserAppComponent,
    AdminDashboardComponent,
    AdminHeaderComponent,
    AdminFooterComponent,
    UserHeaderComponent,
    UserFooterComponent,
    AdminSideMenuComponent,
    LoginComponent,
    UserLoginComponent,
   
  ],
  imports: [
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot() // ToastrModule added
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
