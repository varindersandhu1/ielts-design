﻿import { BaseHttpService } from "../services/base-http-service"
import { Subject, Observable, Subscription, } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { promise } from 'protractor';
import { JsonPipe } from '@angular/common';
// import * as FileSaver from 'file-saver';  
// import * as XLSX from 'xlsx';  
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';  
const EXCEL_EXTENSION = '.xlsx'; 


@Injectable({
    providedIn: 'root',
})
export class AccountService {

    private headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': 'http://localhost:4200/' });

    private UploadImageUrl = environment.ApiUrl + "/FileUpload/UploadData";
    private CreateCataLogUrl = environment.ApiUrl + "/Catalog/Create";
    private DeleteCataLogUrl = environment.ApiUrl + "/Catalog/Delete?CataLogId=";
    private UpdateCataLogUrl = environment.ApiUrl + "/Catalog/Update";
    private GetAllCataLogListUrl = environment.ApiUrl + "/Catalog/Return";

    private CreateArticleUrl = environment.ApiUrl + "/Article/Create";
    private DeleteArticleUrl = environment.ApiUrl + "/Article/Delete?ArticleId=";
    private UpdateArticleUrl = environment.ApiUrl + "/Article/Update";
    private GetAllArticleListUrl = environment.ApiUrl + "/Article/Return?ArticleId=";

    
    private CreateOrderUrl = environment.ApiUrl + "/Order/Create";

//Orders
private GetAlOrdersListUrl = environment.ApiUrl + "/Order/Return";


private GetAllPartiesListUrl = environment.ApiUrl + "/Party/ReturnParties";

    private AdminLoginUrl = environment.ApiUrl + "/UserAuth/UserLoginEmail";
    private CreatePartyUrl = environment.ApiUrl + "/Party/CreateParty";
    private GetUserUrl = environment.ApiUrl + "/Employee/GetUser?EmployeeId=";
    private EmplyeeDeleteUrl = environment.ApiUrl + "/Employee/EmployeeDelete?EmployeeId=";
    private SaveNewPasswordUrl = environment.ApiUrl + "/Employee/PasswordReset?EmployeeId=";
    private GetAllUserTypesUrl = environment.ApiUrl + "/Employee/GetUserTypes?UserTypeId=";
    private CreateEmployeeUrl = environment.ApiUrl + "/Employee/Create";
    private GetContactUsListUrl = environment.ApiUrl + "/Employee/GetContactUsList";
    private ReadQueryUrl = environment.ApiUrl + "/Employee/ReadQuery?Id=";
    private ContactUsDeleteUrl = environment.ApiUrl + "/Employee/ContactUsDelete?ContactUsId=";
    private IsFlagDataCountUrl = environment.ApiUrl + "/Employee/GetContactUsCount";
    private SenEmailContactUsUrl = environment.ApiUrl + "/Employee/Email";
    
    
    private GetOrderByIdUrl = environment.ApiUrl + "/Order/GetByOrderId?OrderId=";

    //Stock
    private GetAllStockDataListUrl = environment.ApiUrl + "/Stock/Return";
    private DispatchSummaryListReturnUrl = environment.ApiUrl + "/Stock/DispatchSummaryListReturn";
    private DispatchListReturnUrl = environment.ApiUrl + "/Stock/DispatchListReturn";
    private GetPartiesByDateUrl = environment.ApiUrl + "/Stock/GetPartiesByDate";

    
    


    private returnResponse: number = 0;
    constructor(private baseHttpService: BaseHttpService,
        private httpClient: HttpClient) { }


// Admin Login


AdminLogin(formData): Observable<any> {
    formData = JSON.stringify(formData)
        return this.httpClient.post<any>(this.AdminLoginUrl, formData, {
            headers: this.headers
        });
}


    UploadImage(formData): Observable<any> {
        formData = JSON.stringify(formData)
        return this.httpClient.post<any>(this.UploadImageUrl, formData, {
            headers: this.headers
        });
    }



    //User CRUD
    GetUser(EmployeeId): Observable<any> {
        return this.httpClient.get<any>(this.GetUserUrl + EmployeeId, {
            headers: this.headers
        });
    }

    EmplyeeDelete(EmployeeId): Observable<any> {
        return this.httpClient.get<any>(this.EmplyeeDeleteUrl + EmployeeId, {
            headers: this.headers
        });
    }

    SaveNewPassKey(EmployeeId,NewPassword): Observable<any> {
        return this.httpClient.get<any>(this.SaveNewPasswordUrl + EmployeeId+"&NewPassword="+NewPassword, {
            headers: this.headers
        });
    }

    CreateEmployee(formData): Observable<any> {
        formData = JSON.stringify(formData)
        return this.httpClient.post<any>(this.CreateEmployeeUrl, formData, {
            headers: this.headers
        });
    }

    GetUsertypes(UserType): Observable<any> {
        return this.httpClient.get<any>(this.GetAllUserTypesUrl + UserType, {
            headers: this.headers
        });
    }

    SendEmailContactUs(formData): Observable<any> {
        formData = JSON.stringify(formData)
        return this.httpClient.post<any>(this.SenEmailContactUsUrl, formData, {
            headers: this.headers
        });
    }


    //End USer CRUD


    ///Cata Log CRUD Start
    GetCatalogList(CataLogId): Observable<any> {
        return this.httpClient.get<any>(`${environment.ApiUrl}/CataLog/Return?CataLogId=` + CataLogId, {
            headers: this.headers
        });
    }

    CreateCataLog(formData): Observable<any> {
        formData = JSON.stringify(formData)
        return this.httpClient.post<any>(this.CreateCataLogUrl, formData, {
            headers: this.headers
        });
    }

    UpdateCataLog(formData): Observable<any> {
        formData = JSON.stringify(formData)
        return this.httpClient.post<any>(this.UpdateCataLogUrl, formData, {
            headers: this.headers
        });
    }
    DeleteCataLog(CataLogId): Observable<any> {
        return this.httpClient.get<any>(this.DeleteCataLogUrl + CataLogId, {
            headers: this.headers
        });
    }
    ///Cata Log CRUD End


    ///Article CRUD Start
    GetArticleList(ArticleId): Observable<any> {
        return this.httpClient.get<any>(this.GetAllArticleListUrl + ArticleId + "&Catalogid=-1", {
            headers: this.headers
        });
    }

    GetAllArticleByCataLogId(Catalogid): Observable<any> {
        let ArticleId = -1;
        return this.httpClient.get<any>(this.GetAllArticleListUrl + ArticleId + "&Catalogid="+Catalogid, {
            headers: this.headers
        });
    }

    CreateArticle(formData): Observable<any> {
        formData = JSON.stringify(formData)
        return this.httpClient.post<any>(this.CreateArticleUrl, formData, {
            headers: this.headers
        });
    }

    UpdateArticle(formData): Observable<any> {
        formData = JSON.stringify(formData)
        return this.httpClient.post<any>(this.UpdateArticleUrl, formData, {
            headers: this.headers
        });
    }
    DeleteArticle(ArticleId): Observable<any> {
        return this.httpClient.get<any>(this.DeleteArticleUrl + ArticleId, {
            headers: this.headers
        });
    }
    ///Article Log CRUD End


    //Orders CRUD
    
    GetOrderList(PartyId,OrderStatus,CreatedBy): Observable<any> {
        return this.httpClient.get<any>(this.GetAlOrdersListUrl + "?PartyId="+PartyId+"&OrderStatus="+OrderStatus+ "&CreatedBy="+CreatedBy, {
            headers: this.headers
        });
    }


    GetOrderById(OrderId): Observable<any> {
        return this.httpClient.get<any>(this.GetOrderByIdUrl+OrderId, {
            headers: this.headers
        });
    }

    GetAllParties(): Observable<any> {
        return this.httpClient.get<any>(this.GetAllPartiesListUrl,  {
            headers: this.headers
        });
    }


   


    CreateParty(formData): Observable<any> {
        formData = JSON.stringify(formData)

        return this.httpClient.post<any>(this.CreatePartyUrl, formData, {
            headers: this.headers
        });
    }

    CreateOrder(formData): Observable<any> {
        formData = JSON.stringify(formData)
        return this.httpClient.post<any>(this.CreateOrderUrl, formData, {
            headers: this.headers
        });
    }

    // End Orders CRUD


    //Stock Return 
    GetAllStockDataList(): Observable<any> {
        return this.httpClient.get<any>(this.GetAllStockDataListUrl , {
            headers: this.headers
        });
    }

    DispatchSummaryListReturn(formData): Observable<any> {
        formData = JSON.stringify(formData)
        return this.httpClient.post<any>(this.DispatchSummaryListReturnUrl, formData, {
            headers: this.headers
        });
    }

    DispatchListReturn(formData): Observable<any> {
        formData = JSON.stringify(formData)
        return this.httpClient.post<any>(this.DispatchListReturnUrl, formData, {
            headers: this.headers
        });
    }

    GetPartiesByDate(formData): Observable<any> {
        formData = JSON.stringify(formData)
        return this.httpClient.post<any>(this.GetPartiesByDateUrl, formData, {
            headers: this.headers
        });
    }

    



    ///Get All Data

    GetContactUsList(): Observable<any> {
        return this.httpClient.get<any>(this.GetContactUsListUrl , {
            headers: this.headers
        });
    }

    ReadQuery(Id): Observable<any> {
        return this.httpClient.get<any>(this.ReadQueryUrl+Id , {
            headers: this.headers
        });
    }

    ContactUsDelete(Id): Observable<any> {
        return this.httpClient.get<any>(this.ContactUsDeleteUrl+Id , {
            headers: this.headers
        });
    }

    IsFlagDataCount(): Observable<any> {
        return this.httpClient.get<any>(this.IsFlagDataCountUrl , {
            headers: this.headers
        });
    }



    // public exportAsExcelFile(json: any[], excelFileName: string): void {  
    //     const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);  
    //     const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };  
    //     const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });  
    //     this.saveAsExcelFile(excelBuffer, excelFileName);  
    //   }  
    //   private saveAsExcelFile(buffer: any, fileName: string): void {  
    //      const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});  
    //      FileSaver.saveAs(data, fileName + '_export_' + new  Date().getTime() + EXCEL_EXTENSION);  
    //   }  


}