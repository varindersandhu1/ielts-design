﻿import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
  })
export class ErrorService {

    private ErrorMessages = []

    constructor() {
        
    }

    GetErrorsFromResponse(errorResponse: any) {
        this.ErrorMessages = [];
        var messages = JSON.parse(errorResponse._body).Message;
        var errors = JSON.parse(messages).messages
        for (var error in errors) {
            this.ErrorMessages.push(errors[error]);
        }
        return this.ErrorMessages;
    }
    GetErrorsFromResponseSignup(errorResponse: any) {
        this.ErrorMessages = [];
        var messages = JSON.parse(errorResponse._body).Message;
        var errors = JSON.parse(messages).ModelState
        for (var error in errors) {
            this.ErrorMessages.push(errors[error]);
        }
        return this.ErrorMessages;
    }
    GetErrorsFromResponseSignin(errorResponse: any) {
        this.ErrorMessages = [];
        var messages = JSON.parse(errorResponse._body).Message;
        var errors = JSON.parse(messages).error_description
        //for (var error in errors) {
            this.ErrorMessages.push(errors);
       // }
        return this.ErrorMessages;
    }
}