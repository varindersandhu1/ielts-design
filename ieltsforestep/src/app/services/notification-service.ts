import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({ providedIn: 'root' })
export class NotificationService {

	constructor(private toastrService: ToastrService) { }



	showSuccess(message) {
		this.toastrService.success(message, 'Success');
	  }

	  showError(message) {
		this.toastrService.error(message, "Error");
	  }
	

}
