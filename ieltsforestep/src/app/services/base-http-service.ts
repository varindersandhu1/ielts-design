﻿import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
//import 'rxjs/add/operator/toPromise';
//import { Cookie } from 'ng2-cookies';

@Injectable({
    providedIn: 'root',
  })
export class BaseHttpService {
    private headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    
    private authHeaderAdded: boolean = false
    constructor(private http: HttpClient) { }

    GetHeaders(): HttpHeaders {
        this.createAuthorizationHeader();

        return this.headers;


    }
    createAuthorizationHeader() {
       // if (Cookie.check("usersCookies")) {
            this.headers.append('Access-Control-Allow-Origin','*');
            this.headers.append('Access-Control-Allow-Methods','GET, POST, PATCH, PUT, DELETE, OPTIONS');
            this.headers.append('Access-Control-Allow-Headers','Origin, Content-Type, X-Auth-Token');
            //this.headers.s    et('Authorization', 'Bearer ' + Cookie.get("usersCookies"));

       // }
        //this.headers.set('Access-Control-Allow-Origin', '*');
    }
    Post(url, model: any) {

        this.createAuthorizationHeader();
        return this.http.post(url, model, { headers: this.headers }).toPromise();
    }

    Put(url, model: any) {
        this.createAuthorizationHeader();
        return this.http.put(url, model, { headers: this.headers }).toPromise();
    }

    Get(url: string) {
        this.createAuthorizationHeader();
        return this.http.get(url, { headers: this.headers }).toPromise();
    }


    Delete(url: string) {
        this.createAuthorizationHeader();
        return this.http.delete(url, { headers: this.headers }).toPromise();
    }
}